﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZupaTest
{
    public class BookingApi
    {
        private IBookingManager _bookingManager;
        public BookingApi()
        {
            var mongoClient = new MongoClient("mongodb://localhost:27017");
            var db = mongoClient.GetDatabase("meetUp");

            var dataProvider = new BookingDataProvider(db);
            _bookingManager = new BookingManager(dataProvider);
        }


        public Task<Tuple<bool,string>> StartBooking(List<BookingData> bookingData)
        {
            return _bookingManager.StartBooking(bookingData);
        }

        public Task<Tuple<bool, string>> CompleteBooking(string bookingId)
        {
            return _bookingManager.CompleteBooking(bookingId);            
        }

        public Task<Tuple<bool, string>> CancelBooking(string bookingId)
        {
            return _bookingManager.CancelBooking(bookingId);
        }

        public Task<Tuple<bool, Booking>> GetBooking(string bookingId)
        {
            return _bookingManager.GetBooking(bookingId);
        }

        public Task<Tuple<bool, List<BookingData>>> GetBookingDataForBooking(string bookingId)
        {
            return _bookingManager.GetBookingDataForBooking(bookingId);
        }
    }
}
