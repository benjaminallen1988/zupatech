﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZupaTest
{
    public class BookingData
    {
        public BookingData()
        {
            BookingDataId = ObjectId.GenerateNewId().ToString();
        }

        [BsonId]
        public string BookingDataId { get; set; } 

        [BsonElement("dateBookedFor")]
        public BsonDateTime DateBookedFor { get; set; }

        [BsonElement("seatId")]
        public string SeatId { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("emailAddress")]
        public string EmailAddress { get; set; }
    }
}
