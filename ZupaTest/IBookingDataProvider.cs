﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZupaTest
{
    public interface IBookingDataProvider
    {
        Task InsertBooking(Booking booking);

        Task InsertBookingData(BookingData bookingData);

        Task UpdateBooking(FilterDefinition<Booking> filterDefinition, UpdateDefinition<Booking> updateDefinition);

        Task DeleteBooking(string bookingId);

        Task DeleteBookingData(string bookingDataId);

        Task<Booking> FindBooking(FilterDefinition<Booking> filterDefinition);

        Task<List<BookingData>> FindBookingData(FilterDefinition<BookingData> filterDefinition);
    }
}
