﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZupaTest
{
    public interface IBookingManager
    {
        Task<Tuple<bool, string>> StartBooking(List<BookingData> bookingData);

        Task<Tuple<bool, string>> CompleteBooking(string bookingId);

        Task<Tuple<bool, string>> CancelBooking(string bookingId);

        Task<Tuple<bool, Booking>> GetBooking(string bookingId);

        Task<Tuple<bool, List<BookingData>>> GetBookingDataForBooking(string bookingId);
    }
}
