﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZupaTest
{
    public enum BookingState
    {
        Started,
        Completed,
    }

    public class Booking
    {
        public Booking()
        {
            BookingId = ObjectId.GenerateNewId().ToString();
            State = BookingState.Started;
            BookingDataIds = new BsonArray { };
        }

        [BsonId]
        public string BookingId { get; set; }

        [BsonElement("bookingDataIds")]
        public BsonArray BookingDataIds { get; set; }

        [BsonElement("dateBooked")]
        public BsonDateTime DateBooked { get; set; }

        [BsonElement("state")]
        public BookingState State { get; set; }        
    }
}
