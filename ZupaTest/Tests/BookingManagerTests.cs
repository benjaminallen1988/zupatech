﻿using MongoDB.Bson;
using MongoDB.Driver;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZupaTest.Tests
{
    [TestFixture]
    class BookingManagerTests
    {
        [Test]
        public async Task BookingManager_StartBooking_Valid_ReturnsTrueAndBookingId()
        {
            var mockDataProvider = new Mock<IBookingDataProvider>();

            mockDataProvider.Setup(dp => dp.FindBookingData(It.IsAny<FilterDefinition<BookingData>>()))
                .ReturnsAsync(new List<BookingData>());

            var bookingManager = new BookingManager(mockDataProvider.Object);

            var bookingData1 = new BookingData
            {
                SeatId = "A1",
                Name = "Test 1",
                EmailAddress = "Test1@Test.Test",
                DateBookedFor = new BsonDateTime(DateTime.Now),

            };

            var bookingData2 = new BookingData
            {
                SeatId = "A2",
                Name = "Test 2",
                EmailAddress = "Test2@Test.Test",
                DateBookedFor = new BsonDateTime(DateTime.Now),

            };

            var result = await bookingManager.StartBooking(new List<BookingData> { bookingData1, bookingData2 });

            Assert.Multiple(() =>
            {
                Assert.That(result.Item1, Is.True);
                Assert.That(result.Item2, Is.Not.EqualTo(string.Empty));
            });
        }

        [Test]
        public async Task BookingManager_StartBooking_SeatAlreadyBooked_ReturnsFalseAndReason()
        {
            var mockDataProvider = new Mock<IBookingDataProvider>();

            mockDataProvider.Setup(dp => dp.FindBookingData(It.IsAny<FilterDefinition<BookingData>>()))
                .ReturnsAsync(new List<BookingData>
                {
                    new BookingData
                    {
                        SeatId = "A1",
                    }
                }
                );

            var bookingManager = new BookingManager(mockDataProvider.Object);

            var bookingData1 = new BookingData
            {
                SeatId = "A1",
                Name = "Test 1",
                EmailAddress = "Test1@Test.Test",
                DateBookedFor = new BsonDateTime(DateTime.Now),

            };

            var bookingData2 = new BookingData
            {
                SeatId = "A2",
                Name = "Test 2",
                EmailAddress = "Test2@Test.Test",
                DateBookedFor = new BsonDateTime(DateTime.Now),

            };

            var result = await bookingManager.StartBooking(new List<BookingData> { bookingData1, bookingData2 });

            Assert.Multiple(() =>
            {
                Assert.That(result.Item1, Is.False);
                Assert.That(result.Item2, Is.EqualTo("You cannot book seat A1 as it is already booked."));
            });
        }


        [Test]
        public async Task BookingManager_StartBooking_NoBookingData_ReturnsFalseAndReason()
        {
            var mockDataProvider = new Mock<IBookingDataProvider>();

            var bookingManager = new BookingManager(mockDataProvider.Object);

            var result = await bookingManager.StartBooking(new List<BookingData> { });

            Assert.Multiple(() =>
            {
                Assert.That(result.Item1, Is.False);
                Assert.That(result.Item2, Is.EqualTo("You cannot book zero seats."));
            });
        }


        [Test]
        public async Task BookingManager_StartBooking_TooManyBookingData_ReturnsFalseAndReason()
        {
            var mockDataProvider = new Mock<IBookingDataProvider>();

            var bookingManager = new BookingManager(mockDataProvider.Object);

            var bookingData = new List<BookingData> {
                new BookingData
                {
                    SeatId = "A1",
                    Name = "Test 1",
                    EmailAddress = "Test1@Test.Test",
                    DateBookedFor = new BsonDateTime(DateTime.Now),

                },
                new BookingData
                {
                    SeatId = "A2",
                    Name = "Test 2",
                    EmailAddress = "Test2@Test.Test",
                    DateBookedFor = new BsonDateTime(DateTime.Now),

                },
                new BookingData
                {
                    SeatId = "A3",
                    Name = "Test 3",
                    EmailAddress = "Test3@Test.Test",
                    DateBookedFor = new BsonDateTime(DateTime.Now),

                },
                new BookingData
                {
                    SeatId = "A4",
                    Name = "Test 4",
                    EmailAddress = "Test4@Test.Test",
                    DateBookedFor = new BsonDateTime(DateTime.Now),

                },
                new BookingData
                {
                    SeatId = "A5",
                    Name = "Test 5",
                    EmailAddress = "Test5@Test.Test",
                    DateBookedFor = new BsonDateTime(DateTime.Now),

                }
            };

            var result = await bookingManager.StartBooking(bookingData);

            Assert.Multiple(() =>
            {
                Assert.That(result.Item1, Is.False);
                Assert.That(result.Item2, Is.EqualTo("You cannot book more than four seats."));
            });
        }

        [Test]
        public async Task BookingManager_StartBooking_DuplicateNames_ReturnsFalseAndReason()
        {
            var mockDataProvider = new Mock<IBookingDataProvider>();

            var bookingManager = new BookingManager(mockDataProvider.Object);

            var bookingData1 = new BookingData
            {
                SeatId = "A1",
                Name = "Test 1",
                EmailAddress = "Test1@Test.Test",
                DateBookedFor = new BsonDateTime(DateTime.Now),

            };

            var bookingData2 = new BookingData
            {
                SeatId = "A2",
                Name = "Test 1",
                EmailAddress = "Test2@Test.Test",
                DateBookedFor = new BsonDateTime(DateTime.Now),

            };

            var result = await bookingManager.StartBooking(new List<BookingData> { bookingData1, bookingData2 });

            Assert.Multiple(() =>
            {
                Assert.That(result.Item1, Is.False);
                Assert.That(result.Item2, Is.EqualTo("You must provide a unique name and email address for each seat."));
            });
        }


        [Test]
        public async Task BookingManager_StartBooking_DuplicateEmails_ReturnsFalseAndReason()
        {
            var mockDataProvider = new Mock<IBookingDataProvider>();

            var bookingManager = new BookingManager(mockDataProvider.Object);

            var bookingData1 = new BookingData
            {
                SeatId = "A1",
                Name = "Test 1",
                EmailAddress = "Test1@Test.Test",
                DateBookedFor = new BsonDateTime(DateTime.Now),

            };

            var bookingData2 = new BookingData
            {
                SeatId = "A2",
                Name = "Test 2",
                EmailAddress = "Test1@Test.Test",
                DateBookedFor = new BsonDateTime(DateTime.Now),

            };

            var result = await bookingManager.StartBooking(new List<BookingData> { bookingData1, bookingData2 });

            Assert.Multiple(() =>
            {
                Assert.That(result.Item1, Is.False);
                Assert.That(result.Item2, Is.EqualTo("You must provide a unique name and email address for each seat."));
            });
        }

        [Test]
        public async Task BookingManager_StartBooking_DuplicateSeats_ReturnsFalseAndReason()
        {
            var mockDataProvider = new Mock<IBookingDataProvider>();

            var bookingManager = new BookingManager(mockDataProvider.Object);

            var bookingData1 = new BookingData
            {
                SeatId = "A1",
                Name = "Test 1",
                EmailAddress = "Test1@Test.Test",
                DateBookedFor = new BsonDateTime(DateTime.Now),

            };

            var bookingData2 = new BookingData
            {
                SeatId = "A1",
                Name = "Test 2",
                EmailAddress = "Test1@Test.Test",
                DateBookedFor = new BsonDateTime(DateTime.Now),

            };

            var result = await bookingManager.StartBooking(new List<BookingData> { bookingData1, bookingData2 });

            Assert.Multiple(() =>
            {
                Assert.That(result.Item1, Is.False);
                Assert.That(result.Item2, Is.EqualTo("You cannot book the same seat more than once."));
            });
        }

        [Test]
        public async Task BookingManager_CancelBooking_BookingExists_ReturnsTrueAndCancelsBooking()
        {
            var mockDataProvider = new Mock<IBookingDataProvider>();

            mockDataProvider.Setup(dp => dp.FindBooking(It.IsAny<FilterDefinition<Booking>>())).ReturnsAsync(new Booking());

            var bookingManager = new BookingManager(mockDataProvider.Object);

            var result = await bookingManager.CancelBooking("TestBookingId");

            Assert.Multiple(() =>
            {
                Assert.That(result.Item1, Is.True);
                Assert.That(result.Item2, Is.EqualTo("Booking cancelled."));
            });
        }

        [Test]
        public async Task BookingManager_CancelBooking_BookingDoesNotExist_ReturnsFalseAndReason()
        {
            var mockDataProvider = new Mock<IBookingDataProvider>();

            var bookingManager = new BookingManager(mockDataProvider.Object);

            var result = await bookingManager.CancelBooking("TestBookingId");

            Assert.Multiple(() =>
            {
                Assert.That(result.Item1, Is.False);
                Assert.That(result.Item2, Is.EqualTo("Unable to cancel booking as it does not appear to exist."));
            });
        }

        [Test]
        public async Task BookingManager_CompleteBooking_BookingExists_ReturnsTrueAndCompletesBooking()
        {
            var mockDataProvider = new Mock<IBookingDataProvider>();

            mockDataProvider.Setup(dp => dp.FindBooking(It.IsAny<FilterDefinition<Booking>>())).ReturnsAsync(new Booking());

            var bookingManager = new BookingManager(mockDataProvider.Object);

            var result = await bookingManager.CompleteBooking("TestBookingId");

            Assert.Multiple(() =>
            {
                Assert.That(result.Item1, Is.True);
                Assert.That(result.Item2, Is.EqualTo("Booking completed."));
            });
        }

        [Test]
        public async Task BookingManager_CompleteBooking_BookingDoesNotExist_ReturnsFalseAndReason()
        {
            var mockDataProvider = new Mock<IBookingDataProvider>();

            var bookingManager = new BookingManager(mockDataProvider.Object);

            var result = await bookingManager.CompleteBooking("TestBookingId");

            Assert.Multiple(() =>
            {
                Assert.That(result.Item1, Is.False);
                Assert.That(result.Item2, Is.EqualTo("Unable to complete booking as it does not appear to exist."));
            });
        }
    }
}
