﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace ZupaTest
{    
    class BookingManager : IBookingManager
    {
        private IBookingDataProvider _bookingDataProvider;

        public BookingManager(IBookingDataProvider bookingDataProvider)
        {
            _bookingDataProvider = bookingDataProvider;
        }

        public async Task<Tuple<bool, string>> StartBooking(List<BookingData> bookingData)
        {
            if (bookingData.Count < 1)
            {
                return Tuple.Create(false, "You cannot book zero seats.");
            }

            if (bookingData.Count > 4)
            {
                return Tuple.Create(false, "You cannot book more than four seats.");
            }

            if (bookingData.Select(d => d.SeatId).Distinct().Count() != bookingData.Count)
            {
                return Tuple.Create(false, "You cannot book the same seat more than once.");
            }

            if (bookingData.Select(d => d.Name).Distinct().Count() != bookingData.Count 
                || bookingData.Select(d => d.EmailAddress).Distinct().Count() != bookingData.Count)
            {
                return Tuple.Create(false, "You must provide a unique name and email address for each seat.");
            }

            var seatBookedFilter = CreateFilterForSeatsBookedCheck(bookingData.First().DateBookedFor, bookingData.Select(bd => bd.SeatId));

            var bookedSeats = await _bookingDataProvider.FindBookingData(seatBookedFilter);
            if (bookedSeats.Any())
            {
                var bookedSeatIds = bookedSeats.Select(bs => bs.SeatId);
                return Tuple.Create(false, $"You cannot book seat{(bookedSeatIds.Count() > 1 ? "s " : " ")}" +
                    $"{string.Join(", ", bookedSeatIds)} as {(bookedSeatIds.Count() > 1 ? "they are" : "it is")} already booked.");
            }

            return await CreateBooking(bookingData);
        }

        public async Task<Tuple<bool, string>> CancelBooking(string bookingId)
        {
            var filter = Builders<Booking>.Filter.Eq(b => b.BookingId, bookingId);

            var booking = await _bookingDataProvider.FindBooking(filter);

            if (booking == null)
            {
                return Tuple.Create(false, "Unable to cancel booking as it does not appear to exist.");
            }

            foreach (string bookingDataId in booking.BookingDataIds)
            {
                await _bookingDataProvider.DeleteBookingData(bookingDataId);
            }

            await _bookingDataProvider.DeleteBooking(bookingId);

            return Tuple.Create(true, "Booking cancelled.");
        }

        public async Task<Tuple<bool, string>> CompleteBooking(string bookingId)
        {
            var filter = Builders<Booking>.Filter.Eq(b => b.BookingId, bookingId);

            var booking = await _bookingDataProvider.FindBooking(filter);

            if (booking == null)
            {
                return Tuple.Create(false, "Unable to complete booking as it does not appear to exist.");
            }

            var update = Builders<Booking>.Update.Set(b => b.State, BookingState.Completed).Set(b => b.DateBooked, new BsonDateTime(DateTime.Now));
            await _bookingDataProvider.UpdateBooking(filter, update);

            return Tuple.Create(true, "Booking completed.");

        }

        public async Task<Tuple<bool, Booking>> GetBooking(string bookingId)
        {
            var filter = Builders<Booking>.Filter.Eq(b => b.BookingId, bookingId);
            var booking = await _bookingDataProvider.FindBooking(filter);

            if (booking == null)
            {
                return Tuple.Create(false, booking);
            }

            return Tuple.Create(true, booking);
        }

        public async Task<Tuple<bool, List<BookingData>>> GetBookingDataForBooking(string bookingId)
        {
            var filter = Builders<Booking>.Filter.Eq(b => b.BookingId, bookingId);
            var booking = await _bookingDataProvider.FindBooking(filter);

            if (booking == null)
            {
                return Tuple.Create(false, new List<BookingData>());
            }

            var bookingDatafilter = Builders<BookingData>.Filter.In(bd => bd.BookingDataId, booking.BookingDataIds);

            var bookingData = await _bookingDataProvider.FindBookingData(bookingDatafilter);

            if (!bookingData.Any()) {
                return Tuple.Create(false, bookingData);
            }

            return Tuple.Create(true, bookingData);
        }

        private FilterDefinition<BookingData> CreateFilterForSeatsBookedCheck(BsonDateTime dateBookedFor, IEnumerable<string> seatIds)
        {
            return Builders<BookingData>.Filter.Eq(bd => bd.DateBookedFor, dateBookedFor)
                & Builders<BookingData>.Filter.In(bd => bd.SeatId, seatIds);
        }

        private async Task<Tuple<bool, string>> CreateBooking(List<BookingData> bookingData)
        {
            var booking = new Booking();

            foreach (var bd in bookingData)
            {
                await _bookingDataProvider.InsertBookingData(bd);
                booking.BookingDataIds.Add(bd.BookingDataId);
            }

            await _bookingDataProvider.InsertBooking(booking);

            return Tuple.Create(true, booking.BookingId.ToString());
        }
    }
}
