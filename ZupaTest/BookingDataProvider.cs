﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace ZupaTest
{
    class BookingDataProvider : IBookingDataProvider
    {
        private IMongoCollection<BookingData> _bookingDataCollection;
        private IMongoCollection<Booking> _bookingCollection;

        public BookingDataProvider(IMongoDatabase bookingDatabase)
        {
            _bookingCollection = bookingDatabase.GetCollection<Booking>("booking");
            _bookingDataCollection = bookingDatabase.GetCollection<BookingData>("bookingData");

        }

        public async Task InsertBooking(Booking booking)
        {
            await _bookingCollection.InsertOneAsync(booking);
        }

        public async Task InsertBookingData(BookingData bookingData)
        {
            await _bookingDataCollection.InsertOneAsync(bookingData);
        }

        public async Task UpdateBooking(FilterDefinition<Booking> filterDefinition, UpdateDefinition<Booking> updateDefinition)
        {
            await _bookingCollection.UpdateOneAsync(filterDefinition, updateDefinition);
        }

        public async Task DeleteBooking(string bookingId)
        {
            var filter = Builders<Booking>.Filter.Eq(b => b.BookingId, bookingId);

            await _bookingCollection.DeleteOneAsync(filter);
        }

        public async Task DeleteBookingData(string bookingDataId)
        {
            var filter = Builders<BookingData>.Filter.Eq(bd => bd.BookingDataId, bookingDataId);

            await _bookingDataCollection.DeleteOneAsync(filter);
        }

        public async Task<Booking> FindBooking(FilterDefinition<Booking> filterDefinition)
        {
            return (await _bookingCollection.FindAsync(filterDefinition)).FirstOrDefault();            
        }

        public async Task<List<BookingData>> FindBookingData(FilterDefinition<BookingData> filterDefinition)
        {
            return await (await _bookingDataCollection.FindAsync(filterDefinition)).ToListAsync();
        }
    }
}
